/******************************************************************************
 * @file    main.c
 * @brief   Главный файл проекта зарядно-разрядного устройства
 * @version v0.1
 * @date    12.08.2020
 * @author  Власовский Алексей Игоревич & Мануйлов Александр Владимирович
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include "stm32f358xx.h"
#include "SysTick_.h"
#include "gpio_.h"
#include "rcc_.h"
#include "serial_.h"
#include "var_.h"
#include "tim_.h"
#include "i2c_.h"
#include "adc_.h"

/*____________________PROTOTYPES____________________*/
void NVIC_tuning(void); //Функция включения необходимых векторов прерываний
void shift_reg_update(int reg[8]); //обновление массива данных сдвигового регистра
void shift_reg_handler(void); //обработчик вывода данных в сдвиговый регистр
void global_var_init(void); //дополнительная инициализация переменных
void clear_RXBuffer(void); //Очистка буфера принятых символов
void RXBuffer_Handler (void); //Обработчик буфера принятых символов после приёма \r
void USART1_TXBuf_append (char *buffer); //Отправка данных в USART1
static char *utoa_cycle_sub(uint32_t value, char *buffer); //Преобразование uint to ascii
int is_equal(char *string1, char *string2, int len); //Функция сравнения двух строк (замена strncmp)
int is_digit(char byte); //функция проверки является ли принятый байт цифрой в ascii кодировке
int get_param(char *buf); //выделение параметров из буфера
int set_v(int port, uint16_t voltage); //выставить напряжение на выходе ЦАП (напряжение)
int set_iz(int port, uint16_t voltage); //выставить напряжение на выходе ЦАП (ток заряда)
int set_ir(int port, uint16_t voltage); //выставить напряжение на выходе ЦАП (ток разряда)
void DAC_update(void); //обновление регистров ЦАПов
void DAC_handler(void); //обработчик обновления ЦАП
void eeprom_write(uint8_t address, uint16_t data); //Запись в EEPROM
int eeprom_read(uint8_t address); //Чтение из EEPROM по адресу
void cell_eeprom_write(uint8_t n); //Запись страницы настроек канала в EEPROM
void cell_eeprom_read(uint8_t n); //Чтение старницы настроек канала из EEPROM
int temp_calc(int N); //Преобразование резултата АЦП в температуру сложной формулой
int kalman_filter(uint16_t y, uint16_t m, float k); //Цифровая фильтрация

int main()
{
	SystemInit();
	global_var_init();
	set_clock_cfgr();
	tim_init();
	SysTick_init();
	gpio_init();
	i2c_init();
	adc_init();
	serial_init();
	shift_reg_update(leds);
	
	//Настройка NVIC выведена отдельно для корректной инициализации
	//Также удобно иметь отдельное место распределения приоритетов
	NVIC_tuning();
	
	//Чтение настроек из памяти для каждого канала
	cell_eeprom_read(0);
	cell_eeprom_read(1);
	cell_eeprom_read(2);
	cell_eeprom_read(3);
	
	GPIOF -> ODR &= ~(1 << 2); //выключим кулер
	//инициализация выводов для сдвигового регистра
	GPIOA -> ODR |= (1 << 4);
	GPIOA -> ODR &= ~(1 << 5 | 1 << 7);
	GPIOA -> ODR |= (1 << 5);
	
	while(1){}
}

/*_______________INTERRUPTS HANDLERS_______________*/

//Прерывание SysTick 100 Гц
//Формула расчёта частоты: 8 000 000 / (SysTick->LOAD + 1) = частота а Гц
//Пример расчёта: 8 000 000 / (79 999 + 1) = 100 Гц
void SysTick_Handler (void)
{
	GPIOC -> ODR |= GPIO_ODR_12; //Мигание тестовым диодом
	shift_reg_handler();
	RXBuffer_Handler();
	DAC_handler();
	
	counter++;
	if (counter >= 2) //Этот if даёт частоту 50 Гц
	{
		counter = 0;
		
		//GPIOC -> ODR ^= GPIO_ODR_12; //Мигание тестовым диодом
		
		//Пересчитаем значения АЦП в параметры канала
		int i = 4;
		while (i --> 0)
		{
			
			//Этап фильтрации измерений
			ADC1_data.ir_filtered[i] = \
			kalman_filter(ADC1_data.ir_filtered[i], ADC1_data.ir[i], filtration_coeff);
			
			ADC1_data.iz_filtered[i] = \
			kalman_filter(ADC1_data.iz_filtered[i], ADC1_data.iz[i], filtration_coeff);
			
			ADC3_data.voltage_filtered[i] = \
			kalman_filter(ADC3_data.voltage_filtered[i], ADC3_data.voltage[i], filtration_coeff);
			
			//ADC3_data.voltage[i] * 1.2773, но работает быстрее
			//cell[i].voltage = (ADC3_data.voltage[i] + (ADC3_data.voltage[i] >> 2) +\
				//(ADC3_data.voltage[i] >> 7) + (ADC3_data.voltage[i] >> 8) + \
				//(ADC3_data.voltage[i] >> 9)) - cell[i].adjust_v;
			
			//на данный момент жёстко закалибровано
			cell[i].voltage = (int) (ADC3_data.voltage_filtered[i] * 1.2773  - 30);
			
			//Защита от переполенения
			if (cell[i].voltage > 5000){ cell[i].voltage = 0; }
			
			//ADC1_data.ir[i] * 0,826 байтовым сдивигом - подстройка нуля
			cell[i].Ir = (ADC1_data.ir_filtered[i] >> 1) + (ADC1_data.ir_filtered[i] >> 2) +\
				(ADC1_data.ir_filtered[i] >> 4) + (ADC1_data.ir_filtered[i] >> 7) - cell[i].adjust_Ir;
			
			//Защита от переполенения
			if (cell[i].Ir > 5000) { cell[i].Ir = 0; }
			
			//ADC1_data.iz[i] * 0,826
			//cell[i].Iz = (ADC1_data.iz[i] >> 1) + (ADC1_data.iz[i] >> 2) +\
				(ADC1_data.iz[i] >> 4) + (ADC1_data.iz[i] >> 7) - cell[i].adjust_Iz;
			
			//на данный момент жёстко закалибровано
			//cell[i].Iz = (int) ((ADC1_data.iz[i] - 229) * 0.9228);
			cell[i].Iz = (int) ADC1_data.iz_filtered[i] * 0.807 - 460; //ТЕСТОВОЕ!
			
			//Защита от переполенения
			if (cell[i].Iz > 5000){ cell[i].Iz = 0; }
			
			//Расчёт температуры через логарифм
			cell[i].temperature = temp_calc(ADC3_data.temp[i]);
			
			//Защита от переполнения при отключённом термистора
			if (cell[i].temperature > 100) { cell[i].temperature = 99; }
		}
	}
	GPIOC -> ODR &= ~GPIO_ODR_12; //Мигание тестовым диодом
};

//Прерывание по переполнению таймера 7 (10 Гц)
//В тестовом режиме 50 Гц (Релоад 199)
void TIM7_IRQHandler (void)
{
	GPIOB -> ODR |= GPIO_ODR_12; //Мигание тестовым диодом
	TIM7 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	
	//Пробежимся по всем ячейкам
	int i = 4;
	while (i --> 0)
	{
		
		//ниже закомментированно для дебага заряда
		/*
		//Если напряжение на ячейке меньше 150 мВ, значит ячейка пустая
		if(cell[i].voltage < 150)
		{
			//cell[i].state = EMPTY;
			cell[i].state = CALM;
		}
		//Если напряжение больше 150 мВ и была EMPTY
		else if((cell[i].voltage > 150) & (cell[i].state == EMPTY))
		{
			cell[i].state = CALM;
		}
		*/
		
		switch (cell[i].state){
				
				case EMPTY:
					//в этом случае выключаем всё
					DAC_v[i] = 0;
					DAC_iz[i] = 0;
					DAC_ir[i] = 0;
					
					DAC_not_updated = 1;
					
					//выключение ИМС заряда (логика инвертируется)
					GPIOE -> ODR |= (1 << (2+i));
				break;
				
				case CALM:
					//в этом случае выключаем всё
					DAC_v[i] = 0;
					DAC_iz[i] = 0;
					DAC_ir[i] = 0;
					
					DAC_not_updated = 1;
					
					//выключение ИМС заряда (логика инвертируется)
					GPIOE -> ODR |= (1 << (2+i));
				break;
				
				case CHARGING_INIT:
					//включим ИМС заряда (логика инвертируется)
					GPIOE -> ODR &= ~(1 << (2+i));
				
					//преобразуем напряжение в уровни ЦАП (множитель 0.8125)
					DAC_v[i] = (cell[i].set_charge_voltage >> 1) + \
						(cell[i].set_charge_voltage >> 2) + \
						(cell[i].set_charge_voltage >> 4);
				
					DAC_iz[i] = cell[i].set_Iz+460; //ТЕСТОВОЕ + 460
				
					//преобразуем напряжение в уровни ЦАП (множитель 0.8125)
					//и сразу добавим немного для индуцирования стартового тока
					//DAC_v[i] = (cell[i].voltage >> 1) + \
							(cell[i].voltage >> 2) + \
							(cell[i].voltage >> 4) + 120;
				
					DAC_not_updated = 1;
				
					//Переведём канал в состояние ЗАРЯЖАЕТСЯ
					cell[i].state = CHARGING;
				break;
				
				case CHARGING:
					
				/*
					//Если ток в канале меньше заданного,
					//то попытаемся стабилизироватьток напряжением на ЦАПе
					if (cell[i].Iz < cell[i].set_Iz)
					{
						DAC_v[i] = DAC_v[i] + 1;
					}
					else
					{
						DAC_v[i] = DAC_v[i] - 1;
					}
					
					//Защита от переполнения 12-ти битных ЦАПов
					if (DAC_v[i] > 4000){ DAC_v[i] = 4000; }

					//Если напряжение на батарейке УЖЕ достаточное, то ЦАП менять не надо
					if (cell[i].voltage >= cell[i].set_charge_voltage)
					{
						DAC_v[i] = cell[i].set_charge_voltage * 0.8191;
					}
					
					//Выставим флаг на обновление ЦАПов
					DAC_not_updated = 1;
					
					//Если новое значение тока меньше, чем на прошлом цикле
					if(cell[i].Iz < cell[i].last_Iz)
					{
						//Если оно изменилось больше, чем на 30%
						if (((cell[i].last_Iz - cell[i].Iz)/cell[i].last_Iz) > 0.3)
						{
							//Заканчиваем заряд
							cell[i].state = CHARGING_END;
						}
					}
					
					//Если напряжение на батарейке равно заданному и ток мал
					// 200 стоит для теста, далее нужна отдельная переменная
					if ((cell[i].voltage >= cell[i].set_charge_voltage) &\
						(cell[i].Iz <= 200))
					{
						//Заканчиваем заряд
						cell[i].state = CHARGING_END;
					}
					
					cell[i].last_Iz = cell[i].Iz; //Запомним значение тока
				*/
				
				break;
				
				case CHARGING_END:
					//выключение ИМС заряда (логика инвертируется)
					GPIOE -> ODR |= (1 << (2+i));
					cell[i].state = CALM;
				break;
		}
	}
	GPIOB -> ODR &= ~GPIO_ODR_12; //Мигание тестовым диодом
}

//Прерывание ADC1 по флагу EOC
void ADC1_2_IRQHandler (void)
{
	//Если мы попадаем в первые 4 преобразования последовательности
	if (ADC1_i >= 0 & ADC1_i <= 3)
	{
		//записываем результаты этих преобразований в ток разряда
		//ADC1_data.ir[ADC1_i] = ((ADC1 -> DR) >> 4) + (ADC1_data.ir[ADC1_i] - (ADC1_data.ir[ADC1_i] >> 4));
		//ADC1_data.ir[ADC1_i] = kalman_filter(ADC1_data.ir[ADC1_i], ADC1 -> DR, 0.4);
		ADC1_data.ir[ADC1_i] = ADC1 -> DR;
	}
	
	//Если мы попадаем в преобразования c 5 по 8-е
	if (ADC1_i >= 4 & ADC1_i <= 7)
	{
		//записываем результаты этих преобразований в ток заряда
		//ADC1_data.iz[ADC1_i - 4] = ((ADC1 -> DR) >> 4) + (ADC1_data.iz[ADC1_i - 4] - (ADC1_data.iz[ADC1_i - 4] >> 4));
		ADC1_data.iz[ADC1_i - 4] = ADC1 -> DR;
	}
	
	ADC1_i++;
	
	//Если после инкрементации счётчик стал равен 8
	if (ADC1_i == 8)
	{
		//Значит последовательность преобразований закончилась
		//и должна начаться снова с SQ1
		
		ADC1_i = 0;
	};
};

//Прерывание ADC3 по флагу EOC
void ADC3_IRQHandler(void)
{
	//Если мы попадаем в первые 4 преобразования последовательности
	if (ADC3_i >= 0 & ADC3_i <= 3)
	{
		//записываем результаты этих преобразований в напряжение заряда
		//ADC3_data.voltage[ADC3_i] = ((ADC3 -> DR) >> 4) + (ADC3_data.voltage[ADC3_i] - (ADC3_data.voltage[ADC3_i] >> 4));
		ADC3_data.voltage[ADC3_i] = ADC3 -> DR;
		
	}
	
	//Если мы попадаем в преобразования c 5 по 8-е
	if (ADC3_i >= 4 & ADC3_i <= 7)
	{
		//записываем результаты этих преобразований в температуру
		//ADC3_data.temp[ADC3_i - 4] = ((ADC3 -> DR) >> 4) + (ADC3_data.temp[ADC3_i - 4] - (ADC3_data.temp[ADC3_i - 4] >> 4));
		ADC3_data.temp[ADC3_i - 4] = ADC3 -> DR;
	}
	
	ADC3_i++;
	
	//Если после инкрементации счётчик стал равен 8
	if (ADC3_i == 8)
	{
		//Значит последовательность преобразований закончилась
		//и должна начаться снова с SQ1
		
		ADC3_i = 0;
	};
};

//Прерывание USART1
void USART1_IRQHandler(void)
{
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		if (TXi_t != TXi_w)
		{
			USART1 -> TDR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		}
		else
		{
			USART1 -> CR1 &= ~(1 << 7); //Запретить прерывания на передачу
		}
	}
	
	//проверяем что был принят бит
	if (USART1 -> ISR & (1 << 5)) //флаг RXNE
	{
		
		RXc = USART1 -> RDR;
		RX_BUF[RXi] = RXc;
		RXi++;

		//Если не символ возврата каретки
		if (RXc != 13) {
			if (RXi > RX_BUF_SIZE-1) {
				clear_RXBuffer();
			}
		}
		else {
			RX_FLAG_END_LINE = 1;
		}
	}
}

//Указание точки входа (костыль по факту)
void Reset_Handler (void)
{
	main();
}

/*____________________FUNCTIONS____________________*/

/**
	* @brief  Инициализация переменных
  * @param  None
  * @retval None
  */
void global_var_init(void)
{
	//Инициализируем диоды еденичками
	uint8_t i = 8;
	while (i --> 0)
	{
		leds[i] = 1;
	};
	
	//Инициализируем данные каналов
	i = 4;
	while (i --> 0)
	{
		DAC_v[i] = 0;
		DAC_iz[i] = 0;
		DAC_ir[i] = 0;
		
		cell[i].set_Ir = 1500;
		cell[i].set_Iz = 1500;
		cell[i].set_charge_voltage = 4200;
		
		cell[i].last_Iz = 0; //инициализация тестовой переменной
		
		cell[i].state = EMPTY;
		
		cell[i].adjust_Ir = 0;
		cell[i].adjust_Iz = 0;
		cell[i].adjust_v = 0;
	};
	
	//Генерация конца строки для HMI
	i = 3;
	while (i --> 0)
	{
		HMI_EOL[i] = 255;
	}
	HMI_EOL[3] = 0;
	
	//Сбросим указатели номера преобразования АЦП
	ADC1_i = 0;
	ADC3_i = 0;
	
	GPIOC -> ODR |= (1 << 8); //Выставим 1 на LDAC
	
	//Выключим все платы защиты
	GPIOC -> ODR &= ~(1 << 4);
	GPIOC -> ODR &= ~(1 << 5);
	GPIOC -> ODR &= ~(1 << 6);
	GPIOC -> ODR &= ~(1 << 7);
	
	//Выключим ИМС заряда
	GPIOE -> ODR &= ~(1 << 2);
	GPIOE -> ODR &= ~(1 << 3);
	GPIOE -> ODR &= ~(1 << 4);
	GPIOE -> ODR &= ~(1 << 5);
	
	//Инициализируем счётчики
	counter = 0;
	counter_2 = 0;
	tim7_counter = 0;
	
	//Коэффициент фильтрации
	filtration_coeff = 0.05;
	
	shift_reg_ready = 1; //флаг готовности регистра
	
	clear_RXBuffer(); //очистим буфер перед работой
	
	TXi_t = 0;
	TXi_w = 0;
	TX_BUF_empty_space = 256; //При инициализациии буфер пуст
}

//Функция включения необходимых векторов прерываний
void NVIC_tuning(void)
{
	NVIC_EnableIRQ(TIM7_IRQn);
	
	NVIC_EnableIRQ(USART1_IRQn);
	
	__disable_fiq();
	NVIC_SetPriority(SysTick_IRQn, 1); //Понижаем приоритет прерывания
	__enable_fiq ();
	
	NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	NVIC_SetPriority(ADC1_2_IRQn, 2); //Понижаем приоритет прерывания
	NVIC_EnableIRQ(ADC1_2_IRQn);
	NVIC_EnableIRQ(ADC3_IRQn);
}

/**
	* @brief  Обновление массива состояния сдвигового регистра
  * @param  Массив состояний диодов
  * @retval None
  */
void shift_reg_update(int reg[])
{
	if (shift_reg_ready)
	{
		
		for (int i = 0; i < 8; i++)
		{
			shift_reg[i] = reg[i];
		}
		
		shift_reg_i = 7;
		shift_reg_ready = 0; //сбросим флаг для инициализации передачи
	}
}

/**
	* @brief  Обработчик для записи данных в сдвиговый регистр
  * @param  None
  * @retval None
  */
void shift_reg_handler(void)
{
	
	//Если флаг сброшен надо передавать данные
	if (!shift_reg_ready)
	{
		
		//Перед началом передачи выставим LOW на защёлке
		if (shift_reg_i == 7){ GPIOA -> ODR &= ~(1 << 4); }
		
		//Переключаем ногу синхросигнала
		GPIOA -> ODR ^= GPIO_ODR_5;
		
		//Если синхросигнал сейчас LOW
		if (!(GPIOA -> ODR & (1 << 5)))
		{
			
			//Если в массиве лежит еденичка
			if(shift_reg[shift_reg_i] == 1)
			{
				//пишем еденичку в данные
				GPIOA -> ODR |= (1 << 7);
			}
			
			//Если не еденичка пишем ноль
			if(shift_reg[shift_reg_i] == 0)
			{
				GPIOA -> ODR &= ~(1 << 7);
			}
			
			//Декрементируем указатель
			shift_reg_i--;
			
		}
		//В конце передачи выставим HIGH на защёлке и выставим флаг
		if (shift_reg_i == -1)
		{
			//Переключаем ногу синхросигнала чтобы задвинуть последний бит
			GPIOA -> ODR ^= GPIO_ODR_5;
			GPIOA -> ODR |= (1 << 4);
			shift_reg_ready = 1;
		}
	}
}

//Преобразование результатов преобразования АЦП в температуру в градусах цельсия
int temp_calc(int N)
{
	float d_U = 0.79;
	float U_out;
	float U_1;
	float R_t;
	float R_t0 = 10;
	float beta = 3988;
	float t0 = 298.15;
	float T_k;
	float T_c;
	float my_lg;
	
	U_out = N*d_U;
	U_1 = U_out * 2;
	R_t = (10 * U_1)/(5000 - U_1);
	
	float y = (R_t/R_t0)-1;
	
	// ln(y)/ln(10) = lg(y)
	my_lg = (y - (y*y/2) + (y*y*y/3) - (y*y*y*y/4))/2.3025;
	
	T_k = (beta*t0)/(beta + (t0*my_lg));
	T_c = T_k - 273.15;
	
	return (int) (T_c);
};

//Обработчик обновления ЦАПов
void DAC_handler(void)
{
	//Если ЦАПы нужно обновить
	if (DAC_not_updated)
	{
		//Перед обновлением сбрасываем флаг NACK
		I2C2 -> ICR |= I2C_ICR_NACKCF;
		//обновляем
		DAC_update();
		
		//Если после обновления не было принято ни одного NACK
		if(!(I2C2 -> ISR & I2C_ISR_NACKF))
		{
			DAC_not_updated = 0;
		}
	}
};

/**
  * @brief  Фильтр Калмана для фильтрации помех
  * @param  y - предыдущее отфильтрованное измерение
  * 		m - текущее измерение
  * 		k - коэффициент фильтрации
  * @retval Возвращает отфильтрованное измерение
  */
int kalman_filter(uint16_t y, uint16_t m, float k)
{
	return (int) ((m*k)+((1-k)*y));
}

//функция обновляет регистры ЦАПов
void DAC_update(void)
{
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x61 << 1); //Slave address = 1100001 = 0x61
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[0] >> 8); //Пишем первый байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_v[0] & 0xFF); //Пишем второй байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[1] >> 8); //Пишем первый байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_v[1] & 0xFF); //Пишем второй байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[2] >> 8); //Пишем первый байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_v[2] & 0xFF); //Пишем второй байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[3] >> 8); //Пишем первый байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_v[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TC или NACKF
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!((I2C2 -> ISR & (1 << 5)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события stop или NACKF
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
	
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x62 << 1); //Slave address = 1100010 = 0x62
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[0] >> 8); //Пишем первый байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_iz[0] & 0xFF); //Пишем второй байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[1] >> 8); //Пишем первый байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_iz[1] & 0xFF); //Пишем второй байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[2] >> 8); //Пишем первый байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_iz[2] & 0xFF); //Пишем второй байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[3] >> 8); //Пишем первый байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_iz[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TC или NACKF
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!((I2C2 -> ISR & (1 << 5)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события stop или NACKF
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
	
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x63 << 1); //Slave address = 1100011 = 0x63
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
		I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[0] >> 8); //Пишем первый байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_ir[0] & 0xFF); //Пишем второй байт первого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[1] >> 8); //Пишем первый байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_ir[1] & 0xFF); //Пишем второй байт второго канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[2] >> 8); //Пишем первый байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_ir[2] & 0xFF); //Пишем второй байт третьего канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[3] >> 8); //Пишем первый байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TXIS или NACKF
	I2C2 -> TXDR = (uint8_t) (DAC_ir[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события TC или NACKF
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!((I2C2 -> ISR & (1 << 5)) | (I2C2 -> ISR & (1 << 4)))); //Дожидаемся события stop или NACKF
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
};

/**
	* @brief  Функция выставляет напряжение на конкретном выходе ЦАП (напряжение)
  * @param  port - номер выхода ЦАП
	* 				voltage - напряжение на этом выходе ЦАП
  * @retval None
  */
int set_v(int port, uint16_t voltage)
{
	
	//Проверка на превышение напряжения
	if (voltage <= 4096)
	{
		DAC_v[port] = voltage - 1; //запишем напряжение в массив
	}
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x61 << 1); //Slave address = 1100001 = 0x61
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[0] >> 8); //Пишем первый байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_v[0] & 0xFF); //Пишем второй байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[1] >> 8); //Пишем первый байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_v[1] & 0xFF); //Пишем второй байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[2] >> 8); //Пишем первый байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_v[2] & 0xFF); //Пишем второй байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_v[3] >> 8); //Пишем первый байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_v[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 6))); //Дожидаемся события TC
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!(I2C2 -> ISR & (1 << 5))); //Дожидаемся события stop detected
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
	
	return 1;
}

/**
	* @brief  Функция выставляет напряжение на конкретном выходе ЦАП (ток заряда)
  * @param  port - номер выхода ЦАП
	* 				voltage - напряжение на этом выходе ЦАП
  * @retval None
  */
int set_iz(int port, uint16_t voltage)
{
	
	//Проверка на превышение напряжения
	if (voltage <= 4096)
	{
		DAC_iz[port] = voltage - 1; //запишем напряжение в массив
	}
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x62 << 1); //Slave address = 1100010 = 0x62
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[0] >> 8); //Пишем первый байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_iz[0] & 0xFF); //Пишем второй байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[1] >> 8); //Пишем первый байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_iz[1] & 0xFF); //Пишем второй байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[2] >> 8); //Пишем первый байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_iz[2] & 0xFF); //Пишем второй байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_iz[3] >> 8); //Пишем первый байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_iz[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 6))); //Дожидаемся события TC
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!(I2C2 -> ISR & (1 << 5))); //Дожидаемся события stop detected
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
	
	return 1;
}

/**
	* @brief  Функция выставляет напряжение на конкретном выходе ЦАП (ток разряда)
  * @param  port - номер выхода ЦАП
	* 				voltage - напряжение на этом выходе ЦАП
  * @retval None
  */
int set_ir(int port, uint16_t voltage)
{
	
	//Проверка на превышение напряжения
	if (voltage <= 4096)
	{
		DAC_ir[port] = voltage - 1; //запишем напряжение в массив
	}
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x63 << 1); //Slave address = 1100011 = 0x63
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x8 << 16); //Отправляем 8 байтов
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[0] >> 8); //Пишем первый байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_ir[0] & 0xFF); //Пишем второй байт первого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[1] >> 8); //Пишем первый байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_ir[1] & 0xFF); //Пишем второй байт второго канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[2] >> 8); //Пишем первый байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_ir[2] & 0xFF); //Пишем второй байт третьего канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	
	I2C2 -> TXDR = (uint8_t) (DAC_ir[3] >> 8); //Пишем первый байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
	I2C2 -> TXDR = (uint8_t) (DAC_ir[3] & 0xFF); //Пишем второй байт четвёртого канала
	while(!(I2C2 -> ISR & (1 << 6))); //Дожидаемся события TC
	
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	while(!(I2C2 -> ISR & (1 << 5))); //Дожидаемся события stop detected
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
	
	return 1;
}

//Тестовая запиь в EEPROM
void eeprom_write(uint8_t address, uint16_t data)
{
	//Перед обновлением сбрасываем флаг NACK
	I2C2 -> ICR |= I2C_ICR_NACKCF;
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x3 << 16); //Отправляем 3 байта
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (address); //Пишем байт адреса слова в памяти
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (data >> 8); //Пишем первый байт данных
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (data & 0xFF); //Пишем второй байт данных
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	
	//Дожидаемся события stop или NACKF
	while(!((I2C2 -> ISR & (1 << 5)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
}

//Запись страницы настроек канала в еепром
void cell_eeprom_write(uint8_t n)
{
	uint8_t w_addr;
	w_addr = 8*n; //Выбор адреса начала записи в зависимости от номера канала
	
	//Перед обновлением сбрасываем флаг NACK
	I2C2 -> ICR |= I2C_ICR_NACKCF;
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x9 << 16); //Отправляем 9 байта
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (w_addr); //Пишем байт адреса слова в памяти
	
	//set_Ir
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_Ir >> 8); //Пишем первый байт данных
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_Ir & 0xFF); //Пишем второй байт данных
	
	//discharge_voltage
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_discharge_voltage >> 8); //Пишем первый байт данных
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_discharge_voltage & 0xFF); //Пишем второй байт данных
	
	//charge_voltage
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_charge_voltage >> 8); //Пишем первый байт данных
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_charge_voltage & 0xFF); //Пишем второй байт данных
	
	//set_Iz
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_Iz >> 8); //Пишем первый байт данных
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (cell[n].set_Iz & 0xFF); //Пишем второй байт данных
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
	
	//Дожидаемся события stop или NACKF
	while(!((I2C2 -> ISR & (1 << 5)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
}

//Чтение из EEPROM двух байт по адресу
int eeprom_read(uint8_t address)
{
	uint16_t byte_h, byte_l; //временные переменные под считываемые данные
	
	//Сначала необходимо передать в еепром адрес слова, с которого надо начать читать
	//Перед обновлением сбрасываем флаг NACK
	I2C2 -> ICR |= I2C_ICR_NACKCF;
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x1 << 16); //Отправляем 1 байт
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (address); //Пишем байт адреса слова в памяти
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	
	//После того как отправили адрес слова, необходимо начать читать
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 |= (1 << 10); //Мастер зарпашивает данные у слейва
	I2C2 -> CR2 |= (0x2 << 16); //Принимаем 2 байта
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	byte_h = I2C2 -> RXDR;
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	byte_l = I2C2 -> RXDR;
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	
	return ((byte_h << 8) + byte_l);
}

//Чтение настроек канала из eeprom
void cell_eeprom_read(uint8_t n)
{
	uint8_t r_addr;
	r_addr = 8*n;
	
	//Сначала необходимо передать в еепром адрес слова, с которого надо начать читать
	//Перед обновлением сбрасываем флаг NACK
	I2C2 -> ICR |= I2C_ICR_NACKCF;
	
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
	I2C2 -> CR2 |= (0x1 << 16); //Отправляем 1 байт
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события TXIS или NACKF
	while(!((I2C2 -> ISR & (1 << 1)) | (I2C2 -> ISR & (1 << 4))));
	I2C2 -> TXDR = (uint8_t) (r_addr); //Пишем байт адреса слова в памяти
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	
	//После того как отправили адрес слова, необходимо начать читать
	I2C2 -> CR2 = 0; //сброс регистра перед работой
	I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
	I2C2 -> CR2 |= (0x50 << 1); //Slave address = 1010000 = 0x50
	I2C2 -> CR2 |= (1 << 10); //Мастер зарпашивает данные у слейва
	I2C2 -> CR2 |= (0x8 << 16); //Принимаем 8 байт
	
	I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_Ir = I2C2 -> RXDR;
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_Ir = ((cell[n].set_Ir << 8) + I2C2 -> RXDR);
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_discharge_voltage = I2C2 -> RXDR;
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_discharge_voltage = ((cell[n].set_discharge_voltage << 8) + I2C2 -> RXDR);
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_charge_voltage = I2C2 -> RXDR;
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_charge_voltage = ((cell[n].set_charge_voltage << 8) + I2C2 -> RXDR);
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_Iz = I2C2 -> RXDR;
	
	//Дожидаемся события RXNE или NACKF
	while(!((I2C2 -> ISR & (1 << 2)) | (I2C2 -> ISR & (1 << 4))));
	cell[n].set_Iz = ((cell[n].set_Iz << 8) + I2C2 -> RXDR);
	
	//Дожидаемся события TC или NACKF
	while(!((I2C2 -> ISR & (1 << 6)) | (I2C2 -> ISR & (1 << 4))));
	
}

/**
	* @brief  Запись данных в циклический буфер на отправку через USART
	* @param  Массив данных, предназначенных на отправку
  * @retval None
  */
void USART1_TXBuf_append (char *buffer)
{
	//Пока в буфере есть данные
	while (*buffer)
	{
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= (1 << 7); //разрешить прерывание на передачу
	
	//блок ниже вроде как не нужен, потому что флаг TXE не может быть сброшен никогда
	/*
	//Если USART ничего не отправляет, то необходимо отправить один бит
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		USART1 -> TDR = TX_BUF[TXi_t];
		TXi_t ++;
		TX_BUF_empty_space++;
	}
	*/
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

//очистка буфера приёмника
void clear_RXBuffer(void) {
	for (RXi=0; RXi<RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RXi = 0;
	
	input_param[0] = 0;
	input_param[1] = 0;
}

/**
	* @brief  Перевод числа в аски символы методом циклического вычитания
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
static char * utoa_cycle_sub(uint32_t value, char *buffer)
{
   if(value == 0)
   {
      buffer[0] = '0';
      buffer[1] = 0;
      return buffer;
   }
   char *ptr = buffer;
   uint8_t i = 0;
   do
   {
      uint32_t pow10 = pow10Table32[i++];
      uint8_t count = 0;
      while(value >= pow10)
      {
         count ++;
         value -= pow10;
      }
      *ptr++ = count + '0';
   }while(i < 10);
   *ptr = 0;
	// удаляем ведущие нули
		while(*buffer == '0')
		{
		++buffer;
		}
   return buffer;
}

/**
	* @brief  Функция сравнения двух строк (замена strncmp)
	* @param  string1 - первая строка
	*					string2 - вторая строка
	*					len - длинна сравнения (количество байт)
	* @retval Возвращает 0, если строки равны
  */
int is_equal(char *string1, char *string2, int len)
{
	while(len --> 0)
	{
		if(*string1++ != *string2++)
		{
			return 1;
		}
	}
	return 0;
};

/**
	* @brief  Функция проверяет по таблице ascii является ли байт цифрой
	* @param  byte - первая строка
	* @retval Возвращает 1, если байт является цифрой
  */
int is_digit(char byte)
{
	if ((byte == 0x30) | (byte == 0x31) | (byte == 0x32) | (byte == 0x33) |\
		(byte == 0x34) | (byte == 0x35) | (byte == 0x36) | (byte == 0x37) |\
	(byte == 0x38) | (byte == 0x39))
	{
		return 1;
	}
	else
	{
		return 0;
	}
};

/**
	* @brief  Поиск в буфере чисел как параметров команды
	* @param  *buf - адрес массива на приём
	* @retval Возвращает 0 при возникновении ошибки
  */
int get_param(char *buf)
{
	uint8_t num_of_params = 2;
	uint8_t i = 0;
	uint8_t pos = 0;
	int value = 0;
	
	//Дойдём до первого пробела
	while(buf[pos] != ' ')
	{
		pos++;
		//Если прошли весь буфер и не нашли пробела
		if (pos >= RX_BUF_SIZE)
		{
			//Выходим из функции
			return 0;
		}
	}
	
	//Начиная со следующего символа начнём вычислять параметры
	while(buf[++pos] != '\r')
	{
		if (!(is_digit(buf[pos]) | (buf[pos] == ' ')))
		{
			USART1_TXBuf_append("Read error!\r");
			clear_RXBuffer();
			return 0;
		}
		
		if (buf[pos] != ' ')
		{
			value = value*10 + (buf[pos] - 48);
		}
		else
		{
			input_param[i] = value;
			i++;
			value = 0;
			if (i > num_of_params -1)
			{
				USART1_TXBuf_append("Read error!\r");
				clear_RXBuffer();
				return 0;
			}
		}
	}
	
	//Запишем параметр, который будет перед концом строки
	input_param[i] = value;
	
	return 1;
};

//Обработка буфера приёма
void RXBuffer_Handler (void)
{
	
	//Если что-то было считано в буфер команды
	if (RX_FLAG_END_LINE == 1) {
		
		//Вычислим параметры у команды
		get_param(RX_BUF);
		
		// Reset END_LINE Flag
		RX_FLAG_END_LINE = 0;
		
		//Включить кулер
		if (is_equal(RX_BUF, "on\r", 3) == 0) {
			GPIOF -> ODR |= (1 << 2);
			USART1_TXBuf_append("Fan ON\r");
		}
		
		//Тестовая команда, прототип запроса от HMI
		if (is_equal(RX_BUF, "get_status", 10) == 0) {
			USART1_TXBuf_append("t1.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].voltage, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			USART1_TXBuf_append("t5.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].Iz, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			USART1_TXBuf_append("t12.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].temperature, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			
			USART1_TXBuf_append("va8.txt=\"");
			switch (cell[input_param[0] - 1].state){
				
				case EMPTY:
					USART1_TXBuf_append("EMPTY");
				break;
				
				case CALM:
					USART1_TXBuf_append("CALM");
				break;
				
				case CHARGING:
					USART1_TXBuf_append("CHARGING");
				break;
			}
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
		}
		
		//запрос состояния для главного меню
		if (is_equal(RX_BUF, "menu_update", 11) == 0) {
			
			int i = 4;
			while (i --> 0)
			{
				USART1_TXBuf_append("va");
				USART1_TXBuf_append(utoa_cycle_sub(i, buffer_));
				USART1_TXBuf_append(".txt=\"");
				
				switch (cell[i].state){
					
					case EMPTY:
						USART1_TXBuf_append(utoa_cycle_sub(0, buffer_));
					break;
					
					case CALM:
						USART1_TXBuf_append(utoa_cycle_sub(1, buffer_));
					break;
					
					case CHARGING:
						USART1_TXBuf_append(utoa_cycle_sub(2, buffer_));
					break;
					
					case DISCHARGE:
						USART1_TXBuf_append(utoa_cycle_sub(5, buffer_));
					break;
				}
				
				USART1_TXBuf_append("\"");
				USART1_TXBuf_append(HMI_EOL);
			}
		}
		
		//установить статус на канал
		if (is_equal(RX_BUF, "set_status ", 11) == 0) {
			cell[input_param[0] - 1].state = input_param[1];
		}
		
		//Обновление настроек на экране
		if (is_equal(RX_BUF, "settings_read", 13) == 0) {
			USART1_TXBuf_append("t201.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].set_charge_voltage, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			USART1_TXBuf_append("t202.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].set_Iz, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			USART1_TXBuf_append("t203.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].set_Ir, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
			
			USART1_TXBuf_append("t204.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(cell[input_param[0] - 1].set_discharge_voltage, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
		}
		
		//изменить напряжение заряда
		if (is_equal(RX_BUF, "change_v", 8) == 0) {
			cell[input_param[0] - 1].set_charge_voltage = input_param[1];
		}
		
		//изменить напряжение отсечки на разряде
		if (is_equal(RX_BUF, "change_dv", 9) == 0) {
			cell[input_param[0] - 1].set_discharge_voltage = input_param[1];
		}
		
		//изменить ток заряда
		if (is_equal(RX_BUF, "change_iz", 9) == 0) {
			cell[input_param[0] - 1].set_Iz = input_param[1];
		}
		
		//изменить ток разряда
		if (is_equal(RX_BUF, "change_ir", 9) == 0) {
			cell[input_param[0] - 1].set_Ir = input_param[1];
		}
		
		//сохранить настройки конкретного канала
		if (is_equal(RX_BUF, "cell_data_w", 11) == 0) {
			cell_eeprom_write(input_param[0]-1);
		}
		
		//Выставить напряжение в ЦАП
		if (is_equal(RX_BUF, "set_v", 5) == 0) {
			set_v(input_param[0], input_param[1]);
			USART1_TXBuf_append("DAC_v updated!\r");
		}
		
		//Выставить напряжение в ЦАП
		if (is_equal(RX_BUF, "set_iz", 6) == 0) {
			set_iz(input_param[0], input_param[1]);
			USART1_TXBuf_append("DAC_iz updated!\r");
		}
		
		//Выставить напряжение в ЦАП
		if (is_equal(RX_BUF, "set_ir", 6) == 0) {
			set_ir(input_param[0], input_param[1]);
			USART1_TXBuf_append("DAC_ir updated!\r");
		}
		
		//Выключить кулер
		if (is_equal(RX_BUF, "off\r", 4) == 0) {
			GPIOF -> ODR &= ~(1 << 2);
			USART1_TXBuf_append("Fan OFF\r");
		}
		
		//Осуществить запись в EEPROM ТЕСТОВОЕ!
		if (is_equal(RX_BUF, "mem_w", 5) == 0) {
			foo = input_param[0];
			eeprom_write(foo_addr, foo);
		}
		
		//Осуществить Чтение из EEPROM ТЕСТОВОЕ!
		if (is_equal(RX_BUF, "mem_r", 5) == 0) {
			foo = eeprom_read(foo_addr);
			USART1_TXBuf_append("t901.txt=\"");
			USART1_TXBuf_append(utoa_cycle_sub(foo, buffer_));
			USART1_TXBuf_append("\"");
			USART1_TXBuf_append(HMI_EOL);
		}
		
		//red
		if (is_equal(RX_BUF, "red\r", 4) == 0) {
			leds[0] = 1;
			leds[1] = 0;
			leds[2] = 1;
			leds[3] = 0;
			leds[4] = 1;
			leds[5] = 0;
			leds[6] = 1;
			leds[7] = 0;
			
			shift_reg_update(leds);
			USART1_TXBuf_append("RED!\r");
		}
		
		//green
		if (is_equal(RX_BUF, "green\r", 6) == 0) {
			leds[0] = 0;
			leds[1] = 1;
			leds[2] = 0;
			leds[3] = 1;
			leds[4] = 0;
			leds[5] = 1;
			leds[6] = 0;
			leds[7] = 1;
			
			shift_reg_update(leds);
			USART1_TXBuf_append("GREEN!\r");
		}
		
		//Данной командой перепрошиваем адреса микросхем
		//(новый адрес зашифрован в последних двух битах) (работает только на 10 кГц)
		if (is_equal(RX_BUF, "set_dac_addr\r", 13) == 0) {
			GPIOC -> ODR |= (1 << 8); //Выставим 1 на LDAC
			
			I2C2 -> CR2 &= ~(1 << 11); //The master operates in 7-bit addressing mode
			I2C2 -> CR2 |= (0x60 << 1); //Slave address = 1100000 = 0x60
			I2C2 -> CR2 &= ~(1 << 10); //Мастер пишет слейву
			I2C2 -> CR2 |= (0x3 << 16); //Отправляем 3 байта
			//I2C2 -> CR2 |= (1 << 25); //AUTOEND = 1
			
			I2C2 -> CR2 |= (1 << 13); //Генерируем СТАРТ
			while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
			
			I2C2 -> TXDR = (uint8_t) 0x61; //Пишем первый байт
			while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
			
			I2C2 -> TXDR = (uint8_t) 0x6E; //Пишем второй байт
			//while(!(I2C2 -> ISR & (1 << 0))); //Дожидаемся события TXE
			//Задержка выполнения кода перед сбросом LDAC между 8 и 9 тиком
			for (int i = 0; i <= 1000; i++){GPIOF -> ODR ^= GPIO_ODR_2;}
			GPIOC -> ODR &= ~(1 << 8); //Выставим 0 на LDAC
			while(!(I2C2 -> ISR & (1 << 1))); //Дожидаемся события TXIS
			
			I2C2 -> TXDR = (uint8_t) 0x6F; //Пишем третий байт
			while(!(I2C2 -> ISR & (1 << 6))); //Дожидаемся события TC
			
			I2C2 -> CR2 |= (1 << 14); //Генерируем СТОП
			while(!(I2C2 -> ISR & (1 << 5))); //Дожидаемся события stop detected
			I2C2 -> ICR |= (1 << 5); //Сбрасываем stop detected
			USART1_TXBuf_append("DAC address was changed!\r");
			
			GPIOF -> ODR &= ~(1 << 2); //выключить кулер на случай если остался включён
		}
		
		clear_RXBuffer();
	}
}
