#include <stm32f358xx.h>
#include <gpio_.h>

/** 
 * @brief  GPIO initialization.
 * @param  none.
 * @return none.
 */
void gpio_init(void)
{
	//Включение тактирования порта F
	RCC -> AHBENR |= RCC_AHBENR_GPIOFEN;
	
	//PF2 PP mode
	GPIOF -> MODER |= (1 << 4); //general purpose output mode
	GPIOF -> MODER &= ~(1 << 5);
	GPIOF -> OTYPER |= 1 << 2; //output open drain
	GPIOF -> PUPDR &= ~(1 << 4); //no pull-up\pull-down
	GPIOF -> PUPDR &= ~(1 << 5);
	
	//Включение тактирования порта A
	RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;
	
	//PA4 PA5 PA7 PP output mode
	GPIOA -> MODER |= (1 << 8 | 1 << 10 | 1 << 14); //general purpose output mode
	
	
	//Включение тактирования порта C
	RCC -> AHBENR |= RCC_AHBENR_GPIOCEN;
	
	//PC8 PP mode
	GPIOC -> MODER |= (1 << 16); //general purpose output mode
	GPIOC -> MODER &= ~(1 << 17);
	
	//PC12 PP mode (тестовый светодиод)
	GPIOC -> MODER |= (1 << 24);
	
	//Включение тактирования порта E
	RCC -> AHBENR |= RCC_AHBENR_GPIOEEN;
	
	//PE2 PE3 PE4 PE5 OD mode (Включение ИМС заряда)
	//Стоит отметить, что схема ИМС инвертирует данную логику
	// 0 в регистре ODR воспримет как 1
	GPIOE -> MODER |= (0x55 << 4); // i/o mode
	GPIOE -> OTYPER |= (1 << 2 | 1 << 3 | 1 << 4 | 1 << 5); //open drain
	
	//Включение тактирования порта B
	RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
	
	GPIOB -> MODER |= (1 << 24);
	/*
	//PB7 PP mode
	GPIOB -> MODER |= (1 << 14);
	GPIOB -> MODER &= ~(1 << 15);
	GPIOB -> OTYPER &= ~(1 << 7);
	GPIOB -> PUPDR &= ~(1 << 14);
	GPIOB -> PUPDR &= ~(1 << 15);
	*/
	
	
}
