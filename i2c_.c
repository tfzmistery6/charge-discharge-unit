#include "stm32f358xx.h"

//Инициализация I2С
void i2c_init(void)
{
	//Сначала необходимо настроить выводы GPIO
	//Включение тактирования порта A
	RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;
	
	//Переключим выводы PA10 PA9 в альтернативный режим
	GPIOA -> MODER |= (1 << 19 | 1 << 21);
	//Настроим выводы в режим AF4
	GPIOA -> AFR[1] |= (1 << 10 | 1 << 6);
	//Включим на выводах максимальную скорость
	GPIOA -> OSPEEDR |= (1 << 21 | 1 << 20 | 1 << 19 | 1 << 18);
	
	//Настроим I2C2
	
	//Включим тактирование I2C
	RCC -> APB1ENR |= (1 << 22);
	RCC -> CFGR3 |= (1 << 5); //Сделаем SYSCLK источником тактового сигнала
	
	I2C2 -> CR1 &= ~(1 << 0); //Сбросим бит PE перед настройкой
	I2C2 -> CR1 |= (1 << 1); // Transmit (TXIS) interrupt enabled
	I2C2 -> CR1 |= (1 << 2); // Receive (RXNE) interrupt enabled
	I2C2 -> CR1 &= ~(1 << 12); //Analog noise filter OFF
	I2C2 -> CR1 &= ~(1 << 8 | 1 << 9 | 1 << 10 | 1 << 11); //Digital filter disabled
	
	/*
	//При тактировании SYSCLK = 8MHz I2C Standart mode (10 kHz)
	I2C2 -> TIMINGR |= (1 << 28); //PRESC = 1
	I2C2 -> TIMINGR |= (0xC7 << 0); //SCLL = C7 (50 us)
	I2C2 -> TIMINGR |= (0xC3 << 8); //SCLH = C3 (49 us)
	I2C2 -> TIMINGR |= (0x2 << 16); //SDADEL = 2 (500 ns)
	I2C2 -> TIMINGR |= (0x4 << 20); //SCLDEL = 4 (1250 ns)
	*/
	
	//При тактировании SYSCLK = 8MHz I2C Fast mode (400 kHz)
	I2C2 -> TIMINGR &= ~(1 << 28); //PRESC = 0
	I2C2 -> TIMINGR |= (0xC << 0); //SCLL = C (1625 ns)
	I2C2 -> TIMINGR |= (0x6 << 8); //SCLH = 6 (875 ns)
	I2C2 -> TIMINGR |= (0x1 << 16); //SDADEL = 1 (125 ns)
	I2C2 -> TIMINGR |= (0x1 << 20); //SCLDEL = 1 (125 ns)
	
	I2C2 -> CR1 &= ~(1 << 17); //Clock stretching disabled
	I2C2 -> CR1 |= (1 << 0); //Выставим бит PE обратно
	
}
