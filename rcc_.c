#include "stm32f358xx.h"

/**
	* @brief  Инициализация тактирования контроллера.
  * @param  None
  * @retval None
  */
void set_clock_cfgr(void)
{
	RCC->CR|=RCC_CR_HSEON; // Включить генератор HSE.
  while (!(RCC->CR & RCC_CR_HSERDY)) {}; // Ожидание готовности HSE.
  RCC->CFGR &=~RCC_CFGR_SW; // Очистить биты SW0, SW1.
  RCC->CFGR |= RCC_CFGR_SW_HSE; // Выбрать HSE для тактирования System Clock
}
