#include "stm32f358xx.h"

void adc_init(void)
{
	/*___________ADC1___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PA0 - ADC1_In1
	* PA1 - ADC1_In2
	* PA2 - ADC1_In3
	* PA3 - ADC1_In4
	* PF4 - ADC1_In5
	* PC0 - ADC12_In6
	* PC1 - ADC12_In7
	* PC2 - ADC12_In8
	*/
	//Включим тактирование GPIO
	RCC -> AHBENR |= (RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOCEN | RCC_AHBENR_GPIOFEN);
	
	//Настроим выводы в аналоговый режим
	GPIOA -> MODER |= (0xFF << 0);
	GPIOF -> MODER |= (0x3 << 8);
	GPIOC -> MODER |= (0x3F << 0);
	
	//Включим тактирование ADC1 и ADC2
	RCC -> AHBENR |= RCC_AHBENR_ADC12EN;
	ADC1_2_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC1 -> CR = 0;//Сбросим регистр CR
	ADC1 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	for (int i = 0; i <= 120; i++){GPIOF -> ODR ^= GPIO_ODR_2;}
	GPIOF -> ODR &= ~(1 << 2);
	
	ADC1 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC1 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC1 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC1 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC1
	ADC1 -> CFGR |= (1 << 13); //continuous mode
	ADC1 -> SQR1 |= (0x7 << 0); // L = 8 преобразований (прим. 0000 == 1 пробразование)
	ADC1 -> SQR1 |= (0x1 << 6); //SQ1 = In1
	ADC1 -> SQR1 |= (0x2 << 12); //SQ2 = In2
	ADC1 -> SQR1 |= (0x3 << 18); //SQ3 = In3
	ADC1 -> SQR1 |= (0x4 << 24); //SQ4 = In4
	ADC1 -> SQR2 |= (0x5 << 0); //SQ5 = In5
	ADC1 -> SQR2 |= (0x6 << 6); //SQ6 = In6
	ADC1 -> SQR2 |= (0x7 << 12); //SQ7 = In7
	ADC1 -> SQR2 |= (0x8 << 18); //SQ8 = In8
	
	ADC1 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 8) | (1 << 7); //SMP2 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 11) | (1 << 10); //SMP3 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 14) | (1 << 13); //SMP4 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 17) | (1 << 16); //SMP5 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 20) | (1 << 19); //SMP6 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 23) | (1 << 22); //SMP7 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 26) | (1 << 25); //SMP8 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 29) | (1 << 28); //SMP9 == 181.5 ticks
	
	ADC1 -> SMPR2 |= (1 << 2) | (1 << 1); //SMP10 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 5) | (1 << 4); //SMP11 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 8) | (1 << 7); //SMP12 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 11) | (1 << 10); //SMP13 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 14) | (1 << 13); //SMP14 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 17) | (1 << 16); //SMP15 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 20) | (1 << 19); //SMP16 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 23) | (1 << 22); //SMP17 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 26) | (1 << 25); //SMP18 == 181.5 ticks
	
	
	ADC1 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	ADC1 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC1_2_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC1_2_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC1 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
	

	/*___________ADC3___________*/
	
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PE7 - ADC3_In13
	* PE10 - ADC3_In14
	* PE11 - ADC3_In15
	* PE12 - ADC3_In16
	* PD11 - ADC34_In8
	* PD12 - ADC34_In9
	* PD13 - ADC34_In10
	* PD14 - ADC34_In11
	*/
	//Включим тактирование GPIO
	RCC -> AHBENR |= (RCC_AHBENR_GPIOEEN | RCC_AHBENR_GPIODEN);
	
	//Настроим выводы в аналоговый режим
	GPIOE -> MODER |= (0x3 << 14);
	GPIOE -> MODER |= (0x3F << 20);
	GPIOD -> MODER |= (0xFF << 22);
	
	//Включим тактирование ADC3 и ADC4
	RCC -> AHBENR |= RCC_AHBENR_ADC34EN;
	ADC3_4_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC3 -> CR = 0;//Сбросим регистр CR
	ADC3 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	for (int i = 0; i <= 120; i++){GPIOF -> ODR ^= GPIO_ODR_2;}
	GPIOF -> ODR &= ~(1 << 2);
	
	ADC3 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC3 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC3 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC3 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC3
	ADC3 -> CFGR |= (1 << 13); //continuous mode
	ADC3 -> SQR1 |= (0x7 << 0); // L = 8 преобразований (прим. 0000 == 1 пробразование)
	ADC3 -> SQR1 |= (0xD << 6); //SQ1 = In13
	ADC3 -> SQR1 |= (0xE << 12); //SQ2 = In14
	ADC3 -> SQR1 |= (0xF << 18); //SQ3 = In15
	ADC3 -> SQR1 |= (0x10 << 24); //SQ4 = In16
	ADC3 -> SQR2 |= (0x8 << 0); //SQ5 = In8
	ADC3 -> SQR2 |= (0x9 << 6); //SQ6 = In9
	ADC3 -> SQR2 |= (0xA << 12); //SQ7 = In10
	ADC3 -> SQR2 |= (0xB << 18); //SQ8 = In11
	
	ADC3 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 8) | (1 << 7); //SMP2 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 11) | (1 << 10); //SMP3 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 14) | (1 << 13); //SMP4 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 17) | (1 << 16); //SMP5 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 20) | (1 << 19); //SMP6 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 23) | (1 << 22); //SMP7 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 26) | (1 << 25); //SMP8 == 181.5 ticks
	ADC3 -> SMPR1 |= (1 << 29) | (1 << 28); //SMP9 == 181.5 ticks
	
	ADC3 -> SMPR2 |= (1 << 2) | (1 << 1); //SMP10 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 5) | (1 << 4); //SMP11 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 8) | (1 << 7); //SMP12 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 11) | (1 << 10); //SMP13 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 14) | (1 << 13); //SMP14 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 17) | (1 << 16); //SMP15 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 20) | (1 << 19); //SMP16 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 23) | (1 << 22); //SMP17 == 181.5 ticks
	ADC3 -> SMPR2 |= (1 << 26) | (1 << 25); //SMP18 == 181.5 ticks
	
	ADC3 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	ADC3 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC3_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC3 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
};
