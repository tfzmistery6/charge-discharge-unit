#include "stm32f358xx.h"

/**
	* @brief  Инициализация USART1
  * @param  None
  * @retval None
  */
void serial_init(void)
{
	RCC -> APB2ENR |= RCC_APB2ENR_USART1EN; //Включаем тактирование USART1
	RCC -> AHBENR |= RCC_AHBENR_GPIOBEN; //Включаем тактирование порта B
	RCC -> CFGR3 |= (1 << 0); //Укажем SYSCLK в качестве источника тактирования
	
	//Настроим ножки PB6 PB7 для работы USART1
	GPIOB -> MODER |= (1 << 13 | 1 << 15); //Альтернативный режим
	//Режим AF7
	GPIOB -> AFR[0] |= (1 << 24 | 1 << 25 | 1 << 26 | 1 << 28 | 1 << 29 | 1 << 30);
	//Высокая скорость для ножек PB6 PB7
	GPIOB -> OSPEEDR |= (1 << 12 | 1 << 13 | 1 << 14 | 1 << 15);
	//Подтяжка PB7 к плюсу
	GPIOB -> PUPDR |= (1 << 14);
	
	//Настройка UASRT1
	USART1 -> CR1 |= (1 << 0); //uasrt enable
	USART1 -> CR1 |= (1 << 2); //Receiver enable
	USART1 -> CR1 |= (1 << 3); //Transmitter enable
	USART1 -> CR1 |= (1 << 5); //RXNE interrupt enable
	USART1 -> BRR = 0x0D0; // Baudrate 38400
	
	USART1 -> RQR |= (1 << 3); //сбросим флаги USART
	
	//NVIC_EnableIRQ(USART1_IRQn);
	
}