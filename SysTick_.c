#include <stm32f358xx.h>
#include <SysTick_.h>

/** 
 * @brief  SysTick initialization.
 * @param  none.
 * @return none.
 */
void SysTick_init(void)
{
	SysTick->CTRL = 0x00; //disable SysTick during setup
	SysTick->LOAD = 0x1387F; //RELOAD value (DEC 79 999)
	SysTick->VAL = 0x9C3F; //clear timer value (DEC 39 999)
	SysTick->CTRL |= (1<<0 | 1<<2 | 1<<1); //freq from core`s freq,; irq enable
}

/** 
 * @brief  Time delay.
 * @param  delay - number of ticks.
 * @return none.
 * Prescaler for SysTick = 8.
 * Uses core`s freq. max_delay = 2^24.
 */
void SysTick_wait(uint32_t delay)
{
	SysTick->LOAD = delay -1; //load with the number of ticks to wait
	SysTick->VAL = 0; //clear timer value
	while(!(SysTick->CTRL & (1<<16))){} //wait until counterflag`s raise
}
