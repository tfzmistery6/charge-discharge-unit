#include "shift_reg.h"

//Переменные для обеспечения работы сдвигового регистра
static int shift_reg[8]; //массив для хранения значений регистра
int shift_reg_i; //итератор массива регистра
_Bool shift_reg_ready = 1; //флаг готовности регистра

/**
	* @brief  Обновление массива состояния сдвигового регистра
  * @param  Массив состояний диодов
  * @retval None
  */
void shift_reg_update(int reg[])
{
	if (shift_reg_ready)
	{
		
		for (int i = 0; i < 8; i++)
		{
			shift_reg[i] = reg[i];
		}
		
		shift_reg_i = 7;
		shift_reg_ready = 0;
	}
}

/**
	* @brief  Обработчик для записи данных в сдвиговый регистр
  * @param  None
  * @retval None
  */
void shift_reg_handler(void)
{
	
	//Если флаг сброшен надо передавать данные
	if (!shift_reg_ready)
	{
		
		//Перед началом передачи выставим LOW на защёлке
		if (shift_reg_i == 7){ GPIOA -> ODR &= ~(1 << 4); }
		
		//Переключаем ногу синхросигнала
		GPIOA -> ODR ^= GPIO_ODR_5;
		
		//Если синхросигнал сейчас LOW
		if (!(GPIOA -> ODR & (1 << 5)))
		{
			
			//Если в массиве лежит еденичка
			if(shift_reg[shift_reg_i] == 1)
			{
				//пишем еденичку в данные
				GPIOA -> ODR |= (1 << 7);
			}
			
			//Если не еденичка пишем ноль
			if(shift_reg[shift_reg_i] == 0)
			{
				GPIOA -> ODR &= ~(1 << 7);
			}
			
			//Декрементируем указатель
			shift_reg_i--;
			
		}
		//В конце передачи выставим HIGH на защёлке и выставим флаг
		if (shift_reg_i == -1)
		{
			//Переключаем ногу синхросигнала чтобы задвинуть последний бит
			GPIOA -> ODR ^= GPIO_ODR_5;
			GPIOA -> ODR |= (1 << 4);
			shift_reg_ready = 1;
		}
	}
}