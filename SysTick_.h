#include <stm32f358xx.h>

/* PROTOTYPES */
void SysTick_init(void);
void SysTick_wait(uint32_t delay);
